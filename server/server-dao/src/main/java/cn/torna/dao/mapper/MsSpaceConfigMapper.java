package cn.torna.dao.mapper;

import cn.torna.dao.entity.MsSpaceConfig;
import com.gitee.fastmybatis.core.mapper.CrudMapper;

/**
 * @author thc
 */
public interface MsSpaceConfigMapper extends CrudMapper<MsSpaceConfig, Long> {

}
